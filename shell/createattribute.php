<?php



/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Shell
 * @copyright  Copyright (c) 2006-2016 X.commerce, Inc. and affiliates (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

require_once 'abstract.php';

/**
 * Magento Compiler Shell Script
 *
 * @category    Mage
 * @package     Mage_Shell
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Shell_Compiler extends Mage_Shell_Abstract
{


    public function getInstaller()
    {
      return  Mage::getModel('eav/entity_setup', 'core_setup');

    }



    protected function addAttribute($data)
    {

        $this->getInstaller()->addAttribute($data[0],
            $data[1],
            array(
                'group'         =>$data[2],
                'input'         => $data[3],
                'type'          => $data[4],
                'label'         => $data[5],
                'backend'       => '',
                'visible'       => true,
                'required'      => false,
                'visible_on_front' => true,
                'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
            ));

        $this->getInstaller()->endSetup();
    }


    protected function deleteAttribute($data)
    {

        $this->getInstaller()->removeAttribute($data[0], $data[1]);
        $this->getInstaller()->endSetup();
    }


    public function checkParam($param,$data)
    {

        switch ($param){
            case 'add' : $this->addAttribute($data); $type='add'; break;
            case 'del' : $this->deleteAttribute($data); $type='del'; break;
            default : echo  $this->usageHelp(); die;
        }
            return $type;
    }






    public function run()
    {
  if($this->getArg('at')){

        $param =isset($_SERVER['argv'][3]) ? $_SERVER['argv'][3] :'';
        $strParamFile =isset($_SERVER['argv'][2]) ? $_SERVER['argv'][2] :'';

        if(empty($param) || empty($strParamFile)){
            echo $this->usageHelp(); die;
        } else {

            $handle = fopen($strParamFile, "r");



            $row=0;

            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {

                $num = count($data);

                $strType =  $this->checkParam($param,$data);
                $row++;
                for ($c=0; $c < $num; $c++) {
                    echo $data[$c] .' '.$strType .' success' . "\n";
                }

            }
            echo 'total '.$row  .' attribute '.$strType;
            die;


        }



        }  else {
                    echo $this->usageHelp();
                }
    }

 
    public function usageHelp()
    {
        return <<<USAGE
Usage:  php -f indexer.php -- [options]
  --at filename add            Added new Attribute to DB from csv file attribute.csv
  --at filename del          Delete  Attribute from DB if Attribute in  file attribute.csv
  help                          This help


USAGE;
    }
}

$shell = new Mage_Shell_Compiler();
$shell->run();
